# **Module 1**

Dans ce module on va commencer par le build d&#39;une image Docker Nginx qui va agir en tant que proxy pour des différentes instances web :

_**Nginx Proxy**_

On va configurer Nginx pour router les requêtes pour /app1 et /app2, respectivement vers @ip:8081 et @ip:8082

```
                  +--- host/  ------> index.html sur @ip:8080
                  |
users --> nginx --|--- host/app1 ---> index.html sur @ip:8081
                  |
                  +--- host/app2 ---> index.html sur @ip:8082
```

Notre fichier de conf va être comme le suivant:

```
server {
    listen       ...;
    ...

    location /mail {
        rewrite ^/mail(/.*)$ $1 break;
        proxy_pass http://@ip:8081;
    }

    location /phpmyadmin {
        rewrite ^/phpmyadmin(/.*)$ $1 break;
        proxy_pass http://@ip:8082;
        proxy_redirect / /phpmyadmin/;
    }
    ...
}
```



_**Docker web**_

Dans cette partie on va préparer les images Docker suivantes:
-  PHP 7
-  NGINX
-  MYSQL
-  PHPMYADMIN
-  SMTP

1. PHP 7:

Cette image doit avoir les extensions suivantes:
mysqli, mcrypt, cli, gd, curl, intl, json, xml, xsl, mbstring, imagick, imap, zip, bcmat
Il y a des extensions qui ont des dépendances avec d'autres packages et d'autres extensions qui ne sont plus fourni avec PHP comme mcrypt et imagick et qui est possible de les ajouter utilisant les dépôts de PECL.

Voici un tableau qui résume les dépendances de chaque extension ajoutée:

|  |  |
| ------ | ------ |
| mcrypt |libmcrypt-dev , libmcrypt |
| XSL | libxslt-dev |
|GD|freetype libjpeg-turbo libpng freetype-dev libjpeg-turbo-dev libpng-dev |
|INTL|icu-dev|
|imagick|imagemagick-dev|
|IMAP|imap-dev|
|ZIP|libzip-dev|

Pour cette image j'ai choisi de me baser sur [php:7.4-fpm-alpine](https://hub.docker.com/layers/php/library/php/7.4-fpm-alpine/images/sha256-c3b668772e5a3557921e368a7f2ec57ff6c5441af49ed8e562bdb4781a2b7ff2?context=explore) et j'ai ajouté le fichier php.ini dans le build de l'image (je n'étais pas sûr si c'était demandé de l'ajouter en tant que volume ou non) .


2. NGINX:

Dans cette partie on va se baser sur l'image [nginx:1.19.4-alpine](https://hub.docker.com/layers/nginx/library/nginx/1.19.4-alpine/images/sha256-c7186e945f39dbcde3f528a834e81e697737ce84d304be5b67ccacc3d7712025?context=explore ) , on va ajouter un simple fichier index.php et on va juste modifier le fichier conf de Nginx pour utiliser la socket du conteneur php.


3. SMTP:

Dans cette partie on va préparer une image Docker pour un serveur Postfix.
Postfix c'est un MTA qui va agir comme notre relay SMTP pour router et transférer nos emails vers leurs convenables destinations.

On va se baser sur une image alpine, on va installer Postfix, ajouter un entrypoint qui va configurer Postfix pendant le startup, exposer le port 25 et définir notre CMD qui va être `postfix start-fg`

J'ai utilisé smtp.topnet.tn comme relay host (il nécessite pas d'authentification) et si vous voulez utiliser un smtp qui nécessite l'authentification il faut décommenter les lignes suivantes dans docker-entrypoint.sh :

```
echo "$POSTFIX_RELAY_HOST $POSTFIX_RELAY_USER:$POSTFIX_RELAY_PASSWORD" >> /etc/postfix/sasl_passwd
postmap hash:/etc/postfix/sasl_passwd
postconf -e "smtp_sasl_auth_enable=yes"
postconf -e "smtp_sasl_password_maps=hash:/etc/postfix/sasl_passwd"
postconf -e "smtp_sasl_security_options=noanonymous"  
```


4. MySQL et phpMyAdmin:

Pour [MySQL](https://hub.docker.com/_/mysql) et [phpMyAdmin](https://hub.docker.com/_/phpmyadmin) on va utiliser les images Docker officielles.

_**Docker Compose**_

Après avoir préparer nos images, on peut maintenant lancer notre application web et phpMyAdmin.
L'application web fait rien que envoyer des emails vers l'adresse fedi.haddar@iit.ens.tn.

J'ai mis les étapes et tout ce qui est nécessaire pour orchestrer et lancer les conteneurs dans le fichier docker-compose.

```
git clone https://gitlab.com/545t3c/module1.git
docker-compose -f module1/docker-compose.yml up -d
```
Pour acceder au mailer: http://@ip/mail

Pour acceder à phpMyAdmin: http://@ip/phpmyadmin
